Glacier.nvim
============

glacier.nvim is a no-nonsense neovim colorscheme in lua based on the great [iceberg](https://cocopon.github.io/iceberg.vim/) colorscheme, created by [cocopon](https://cocopon.me/).
The problem with most colorschemes, for me personally, are

- They lack contrast. Often done for esthetic purposes, But this makes code harder to read. Worst offenders are those solarized themes.
- They use too many different colors, sometimes to the point that a normal source file barely has any character in the default foreground color. This makes your files look like unicorn-vomit.

glacier.nvim will try to not over-exagerate the amount of coloring it does of less important syntax elements, while at the same time keeping contrast and readability high.
It is written in lua to provide better customizability and extensibility.
One can easily add overrides and extensions, e.g. highlighting for groups not accounted for, with the colorscheme's chosen colors.

TODOs
=====

- full color scheme
- add debug option that lists known highlight groups that aren't mentioned in the colorscheme
- add option to add a custom, external theme lua file as in the theme dir that will loaded like the other themes lua files.
