-- is this called when colorscheme cmd is run?

if vim.g.glacier_debug == true then
    package.loaded["glacier_nvim"]                 = nil
    package.loaded["glacier_nvim.theme.editor_hl"] = nil
    package.loaded["glacier_nvim.theme.syntax_hl"] = nil
    package.loaded["glacier_nvim.theme"]           = nil
    package.loaded["glacier_nvim.colors"]          = nil
    package.loaded["glacier_nvim.config"]          = nil
end

local glacier_nvim = require'glacier_nvim'
glacier_nvim.load ()
vim.api.nvim_command("redraw")
