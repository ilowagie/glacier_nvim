local config = {}

local defaults = {
    -- extra styles. see highlight-args for possibilities
    styles = {
        comments    = "italic",
        strings     = "NONE",
        diagnostics = "underdotted",
        functions   = "bold",
        keywords    = "NONE",
        variables   = "NONE",
    },
    override = {
        background = true, -- override terminal's bg color
        float_background = true, -- override background of floating windows
        cursorline = true, -- override cursorline color
    },
    -- overrides for highlighting
    --  use Name = { fg = ... }, this will call vim.api.nvim_set_hl (0, Name, { fg = ... })
    --  TODO: allow for usage of glacier_nvim.colors
    highlight_overrides = {},
}

config.options = {}

function config.set_options(opts)
    vim.api.nvim_echo({{"setting options...", "Normal"}}, false, {})
    config.options = vim.tbl_deep_extend("force", config.options, opts or {})
end

config.set_options(defaults)

return config
