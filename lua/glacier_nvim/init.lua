local config = require'glacier_nvim.config'
local theme = require'glacier_nvim.theme'

local glacier_nvim = {}

function glacier_nvim.setup (options)
    config.set_options (options)
    glacier_nvim.load ()
    vim.api.nvim_command("redraw")
end

function glacier_nvim.load ()
    vim.cmd ("hi clear")
    if vim.fn.exists ("syntax on") then
        vim.cmd ("syntax reset")
    end

    if vim.g.glacier_debug then
        -- clear everything, start from a completely clean slate
        for n, _ in pairs(vim.api.nvim__get_hl_defs(0)) do
            vim.api.nvim_set_hl(0, n, {})
        end
    end

    local colors = require'glacier_nvim.colors'

    if not config.options.override.background then
        colors.bg = colors.none
    end
    if not config.options.override.float_background then
        colors.bg_float = colors.none
    end

    theme.load (colors)
    theme.apply(config)
end

return glacier_nvim
