local colors = {
    fg                = "#d0d0d0",
    fg_low_contrast   = "#6c6c6c",
    bg                = "#1c1c1c",
    bg_reverse        = "#1c1c1c",
    bg_low_contrast_0 = "#1e1e1e",
    bg_low_contrast_1 = "#212121",
    bg_low_contrast_2 = "#272727",
    bg_low_contrast_3 = "#2d2d2d",
    light_red         = "#df462e",
    red               = "#e27878",
    yellow            = "#e2a378",
    green             = "#b4be82",
    accent_1          = "#84a0c6",
    accent_2          = "#89b8c2",
    accent_3          = "#e2a378",
    accent_4          = "#b4be82",
    rare_color_1      = "#a093c7",
    debug_color       = "#ff00eb",
    none              = "NONE",
}

colors.good = colors.green
colors.bad = colors.red
colors.warn = colors.yellow

return colors
