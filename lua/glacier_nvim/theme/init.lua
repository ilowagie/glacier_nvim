local highlight_group_modules = {
    editor_hl     = require'glacier_nvim.theme.editor_hl',
    syntax_hl     = require'glacier_nvim.theme.syntax_hl',
    treesitter_hl = require'glacier_nvim.theme.treesitter_hl',
    lsp_hl        = require'glacier_nvim.theme.lsp_hl',
    cmp_hl        = require'glacier_nvim.theme.cmp_hl',
}

local theme = {
    highlight_groups = {}
}

theme.load = function (colors)
    vim.api.nvim_echo({{"loading theme", "Normal"}}, false, {})
    for n, mod in pairs (highlight_group_modules) do
        theme.highlight_groups[n] = mod.load (colors)
    end
end

theme.apply = function (config)
    vim.api.nvim_echo({{"applying theme", "Normal"}}, false, {})
    for _, group in pairs (theme.highlight_groups) do
        for key, highlight in pairs (group) do
            vim.api.nvim_set_hl (0, key, highlight)
        end
    end
    for key, highlight_override in pairs (config.options.highlight_overrides) do
        vim.api.nvim_set_hl (0, key, highlight_override)
    end
end

return theme
