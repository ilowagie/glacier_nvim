local editor_hl = {
    load = function (colors)
        local hl = {
            Cursor         = { fg = colors.fg, bg = colors.none, reverse = true },
            CursorIM       = { fg = colors.debug_color, bg = colors.debug_color, reverse = true },
            CursorLine     = { bg = colors.bg_low_contrast_1 },
            CursorColumn   = { link = "CursorLine" },
            CursorLineNr   = { link = "LineNr" },
            LineNr         = { fg = colors.fg, bg = colors.bg_low_contrast_2 },
            LineNrAbove    = { fg = colors.fg_low_contrast, bg = colors.bg_low_contrast_0 },
            LineNrBelow    = { link = "LineNrAbove" },

            Normal         = { fg = colors.fg, bg = colors.bg },
            NormalFloat    = { fg = colors.fg, bg = colors.bg_low_contrast_1 },
            FloatBorder    = { fg = colors.fg_low_contrast, bg = colors.fg_low_contrast },

            MatchParen     = { bg = colors.bg_low_contrast_1 },

            -- Search: last search pattern highlight
            Search         = { fg = colors.fg, bg = colors.bg_low_contrast_1, bold = true },
            IncSearch      = { fg = colors.fg, bg = colors.none, reverse = true },
            CurSearch      = { link = "IncSearch" },

            -- TabLine = line where tabs are shown
            TabLine        = { fg = colors.fg_low_contrast, bg = colors.bg_low_contrast_2 },
            TabLineFill    = { link = "TabLine" },
            TabLineSel     = { fg = colors.bg_reverse, bg = colors.accent_1 },

            StatusLine     = { fg = colors.fg, bg = colors.none, reverse = true },
            StatusLineTerm = { link = "StatusLine" },
            -- NC = Non-Current, i.e. non-focused in vim (not window in WM)
            StatusLineNC   = { link = "Tabline" },
            TabLineNC      = { link = "StatusLineNC" },

            ModeMsg        = { fg = colors.fg_low_contrast_2, bold = true },

            Visual         = { bg = colors.fg_low_contrast_1, reverse = true },
            VisualNOS      = { link = "Visual" },
            Conceal        = { fg = colors.accent_1, bg = colors.bg_low_contrast_2 },

            Directory      = { fg = colors.accent_1, bg = colors.bg },
            SpecialKey     = { fg = colors.light_red, bg = colors.bg },

            Pmenu          = { fg = colors.fg, bg = colors.bg_low_contrast_1 }, -- vim's builtin autocomplete
            PmenuSel       = { fg = colors.fg, bg = colors.none, reverse = true },
            PmenuSbar      = { fg = colors.accent_3, bg = colors.bg },
            PmenuThumb     = { fg = colors.accent_2, bg = colors.bg },

            ErrorMsg       = { fg = colors.bad, bg = colors.bg, bold = true },
            WarningMsg     = { fg = colors.warn, bg = colors.bg },
            Warnings       = { fg = colors.warn, bg = colors.bg },
            HealthError    = { fg = colors.bad, bg = colors.bg, bold = true },
            HealthSucces   = { fg = colors.good, bg = colors.bg },
            HealthWarning  = { fg = colors.warn, bg = colors.bg },

            SpellBad       = { sp = colors.bad, undercurl = true },
            SpellRare      = { sp = colors.good, undercurl = true },
            SpellLocal     = { sp = colors.warn, undercurl = true },
            SpellCap       = { link = "SpellRare" }, -- if word not Capitalized when it should

            EndOfBuffer = { fg = colors.fg_low_contrast, bg = colors.bg },
            NonText = { fg = colors.fg_low_contrast, bg = colors.bg },

            TermCursor = { link = "Cursor" },
            TermCursorNC = { fg = colors.fg_low_contrast, bg = colors.none, reverse = true },

            NvimInternalError = { fg = colors.red, bg = colors.bg },
            Question = { fg = colors.yellow, bg = colors.bg },
            MoreMsg = { fg = colors.fg_low_contrast, bg = colors.bg },

            Title = { fg = colors.fg, bg = colors.bg, bold = true },

            Folded = { fg = colors.accent_2, bg = colors.bg },
            FoldColumn = { fg = colors.accent_2, bg = colors.bg_low_contrast_0 },

            netrwGray = { fg = colors.fg_low_contrast, bg = colors.bg },

            --  TODO: RedrawDebug: what even is this?
            --          -> RedrawDebugNormal
            --          -> RedrawDebugClear
            --          -> RedrawDebugComposed
            --          -> RedrawDebugRecompose
        }

        return hl
    end
}

return editor_hl
