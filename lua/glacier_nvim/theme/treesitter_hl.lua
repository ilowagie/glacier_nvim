local editor_hl = {
    load = function (colors)
        local hl = {
            TSComment      = { link = "Comment" },
            TSConstant     = { link = "Constant" },
            TSFloat        = { link = "Float" },
            TSNumber       = { link = "Number" },
            TSBoolean      = { link = "Boolean" },
            TSCharacter    = { link = "character" },
            TSString       = { link = "String" },
            TSType         = { link = "Type" },
            TSConditional  = { link = "Conditional" },
            TSFunction     = { link = "Function" },
            TSOperator     = { link = "Operator" },
            TSLabel        = { link = "Label" },
            TSException    = { link = "Exception" },
            TSTag          = { link = "Tag" },
            TSTagDelimiter = { link = "TSTag" },
            TSUnderlined   = { link = "Underlined" },
            TSError        = { link = "Error" },
            TSVariable     = { link = "Identifier" },
            TSKeyword      = { link = "Keyword" },
            TSRepeat       = { link = "Repeat" },

            ["@comment"]     = { link = "TSComment" },
            ["@constant"]    = { link = "TSConstant" },
            ["@float"]       = { link = "TSFloat" },
            ["@number"]      = { link = "TSNumber" },
            ["@boolean"]     = { link = "TSBoolean" },
            ["@character"]   = { link = "TSCharacter" },
            ["@string"]      = { link = "TSString" },
            ["@type"]        = { link = "TSType" },
            ["@conditional"] = { link = "TSConditional" },
            ["@function"]    = { link = "TSFunction" },
            ["@operator"]    = { link = "TSOperator" },
            ["@label"]       = { link = "TSLabel" },
            ["@exception"]   = { link = "TSException" },
            ["@error"]       = { link = "TSError" },
            ["@variable"]    = { link = "TSVariable" },
            ["@keyword"]     = { link = "TSKeyword" },
            ["@repeat"]      = { link = "TSRepeat" },

            TSConstMacro = { fg = colors.rare_color_1, bg = colors.bg, bold = true },
            ["@constant.macro"] = { link = "TSConstMacro" },
            TSKeywordFunction = { fg = colors.accent_4, bg = colors.bg, bold = false },
            ["@keyword.function"] = { link = "TSKeywordFunction" },
            TSFuncBuiltin = { fg = colors.accent_4, bg = colors.bg, bold = false },
            ["@function.builtin"] = { link = "TSFuncBuiltin" },
            TSConstBuiltin = { fg = colors.accent_4, bg = colors.bg, bold = false },
            ["@constant.builtin"] = { link = "TSConstBuiltin" },
            TSVariableBuiltin = { fg = colors.accent_4, bg = colors.bg, bold = true },
            ["@variable.builtin"] = { link = "TSVariableBuiltin" },
            ["@variable.global"] = { fg = colors.rare_color_1, bg = colors.bg, bold = false },
            TSKeywordReturn = { fg = colors.accent_1, bg = colors.bg, bold = true },
            ["@keyword.return"] = { link = "TSKeywordReturn" },
            TSStringEscape = { fg = colors.accent_4, bg = colors.bg, bold = false },
            ["@string.escape"] = { link = "TSStringEscape" },
            TSKeywordOperator = { link = "TSOperator" },
            ["@keyword.operator"] = { link = "TSKeywordOperator" },

            -- TSField
            -- TSProperty
            -- ["@field"]
            -- ["@property"]

            -- TODO:
            --
            -- TSTitle
            -- ["@text.title"]
            -- ["@text.strong"]
            --
            -- TSMethod
            -- TSNamespace
            -- TSStringRegex
            -- ["@method"]
            -- ["@namespace"]
            -- ["@string.regex"]

            -- TODO: handle function calls differently from function declarations/prototypes
            --          -> names in prototypes/declarations should be Normal
            --          -> names in calls should have highlight as currently set for Function
        }

        return hl
    end
}

return editor_hl
