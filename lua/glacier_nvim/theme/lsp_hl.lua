local lsp_hl = {
    load = function (colors)
        local hl = {
            -- diagnostics
            -- signs
            SignColumn = { bg = colors.bg_low_contrast_0 },
            LspDiagnosticSignError = { fg = colors.red, bg = colors.bg_low_contrast_0, bold = true },
            LspDiagnosticSignWarning = { fg = colors.yellow, bg = colors.bg_low_contrast_0, bold = true },
            LspDiagnosticSignInformation = { fg = colors.accent_1, bg = colors.bg_low_contrast_0, bold = true },
            LspDiagnosticSignHint = { fg = colors.green, bg = colors.bg_low_contrast_0, bold = true },
            DiagnosticSignError = { link = "LspDiagnosticSignError" },
            DiagnosticSignWarning = { link = "LspDiagnosticSignWarning" },
            DiagnosticSignInformation = { link = "LspDiagnosticSignInformation" },
            DiagnosticSignHint = { link = "LspDiagnosticSignHint" },
            -- messages in floating windows
            LspDiagnosticFloatingError = { fg = colors.red },
            LspDiagnosticFloatingWarning = { fg = colors.yellow },
            LspDiagnosticFloatingInformation = { fg = colors.accent_1 },
            LspDiagnosticFloatingHint = { fg = colors.green },
            DiagnosticError = { fg = colors.red },
            DiagnosticWarning = { fg = colors.yellow },
            DiagnosticInformation = { fg = colors.accent_1 },
            DiagnosticHint = { fg = colors.green },
        }

        return hl
    end
}

return lsp_hl
