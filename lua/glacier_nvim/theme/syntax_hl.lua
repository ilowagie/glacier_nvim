local syntax_hl = {
    load = function (colors)
        local hl = {
            Comment = { fg = colors.fg_low_contrast, bg = colors.bg, italic = true },
            SpecialComment = { fg = colors.fg_low_contrast, bg = colors.bg, italic = true, bold = true },

            String = { fg = colors.accent_3, bg = colors.bg },
            SpecialChar = { fg = colors.accent_3, bg = colors.bg },
            Constant = { fg = colors.rare_color_1, bg = colors.bg },
            Character = { link = "Constant" },
            Number = { link = "Constant" },
            Boolean = { link = "Constant" },
            Float = { link = "Constant" },

            Structure = { fg = colors.accent_1, bg = colors.bg },
            StorageClass = { fg = colors.accent_4, bg = colors.bg },
            Type = { fg = colors.accent_1, bg = colors.bg },
            Typedef = { fg = colors.accent_4, bg = colors.bg },

            Identifier = { fg = colors.fg, bg = colors.bg, bold = false },
            Conditional = { fg = colors.accent_1, bg = colors.bg },
            Function = { fg = colors.accent_2, bg = colors.bg, bold = false },
            Keyword = { fg = colors.accent_1, bg = colors.bg },
            Repeat = { link = "Conditional" }, -- keywords related with loops, like while and for
            Operator = { fg = colors.accent_4, bg = colors.bg },
            Statement = { fg = colors.accent_1, bg = colors.bg, bold = false },
            Label = { fg = colors.accent_4, bg = colors.bg, bold = false },

            Macro = { fg = colors.rare_color_1, bg = colors.bg, bold = true },
            PreProc = { fg = colors.accent_2, bg = colors.bg, bold = true },
            PreCondit = { link = "PreProc" },
            Define = { link = "PreProc" },
            Include = { fg = colors.accent_1, bg = colors.bg },

            Debug = { fg = colors.fg_low_contrast, bg = colors.bg, bold = true },
            Ignore = { link = "Debug" },

            Delimiter = { link = "Normal" },
            Special = { link = "Normal" },

            Exception = { fg = colors.red, bg = colors.bg },

            Tag = { fg = colors.accent_4, bg = colors.bg, bold = true }, -- like in html tags

            Underlined = { fg = colors.accent_1, bg = colors.bg, underline = true },

            Error = { sp = colors.red, undercurl = false },
            Todo = { fg = colors.rare_color_1, bg = colors.bg, bold = true },

            htmlLink = { link = "Underlined" },
            htmlH1 = { fg = colors.accent_3, bg = colors.bg, bold = true },
            htmlH2 = { fg = colors.accent_2, bg = colors.bg, bold = true },
            htmlH3 = { link = "htmlH2" },
            markdownH1 = { link = "htmlH1" },
            markdownH2 = { link = "htmlH2" },
            markdownH3 = { link = "htmlH3" },
            markdownH1Delimiter = { link = "htmlH1" },
            markdownH2Delimiter = { link = "htmlH2" },
            markdownH3Delimiter = { link = "htmlH3" },

            DiffAdd = { fg = colors.green, bg = colors.bg, bold = true },
            DiffDelete = { fg = colors.red, bg = colors.bg, bold = true },
            DiffChange = { fg = colors.gree, bg = colors.bg, bold = false },
            DiffText = { link = "Comment" },
        }

        return hl
    end
}

return syntax_hl
