local cmp_hl = {
    load = function (colors)
        local hl = {
            CmpItemMenu = { fg = colors.fg, bg = colors.bg_low_contrast_1, italic = true },
            CmpItemMenuDefault = { link = "CmpItemMenu" },
            CmpItemKind = { fg = colors.accent_3, bg = colors.bg_low_contrast_1, },
            CmpItemKindDefault = { link = "CmpItemKind" },
            CmpItemAbbr = { fg = colors.fg, bg = colors.bg_low_contrast_1 },
            CmpItemAbbrMatch = { fg = colors.accent_2, bg = colors.bg_low_contrast_1, bold = true },
            CmpItemAbbrMatchFuzzy = { fg = colors.accent_2, bg = colors.bg_low_contrast_1, bold = false },
            CmpItemAbbrMatchDefault = { fg = colors.accent_2, bg = colors.bg_low_contrast_1, bold = false },
        }

        return hl
    end
}

return cmp_hl
